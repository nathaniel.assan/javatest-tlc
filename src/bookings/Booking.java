package bookings;

import ens_hotel.ENSHotel;
import rooms.exceptions.RoomAlreadyRegisteredException;
import rooms.Room;

import java.util.HashMap;

public class Booking implements ENSHotel {

    private HashMap<String, Room> bookedRooms   =  new HashMap<>();

    public HashMap<String, Room> getBookedRooms() {
        return bookedRooms;
    }

    @Override
    public void addRoom(Room room) throws RoomAlreadyRegisteredException {

    }

    @Override
    public void bookRoom(Room room, int numOfGuests, boolean includeBreakfast) {

    }

    @Override
    public int totalGuestsForToday() {
        return 0;
    }

    @Override
    public double totalValueOfBookingsForToday() {
        return 0;
    }


}
