package Guests;

public class Guest {
    private int guestId;
    private String guestName;
    private String bookingId;

    public Guest(int guestId, String guestName, String bookingId) {
        this.guestId = guestId;
        this.guestName = guestName;
        this.bookingId = bookingId;
    }
}
