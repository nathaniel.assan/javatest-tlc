package ens_hotel;

public interface RoomPricingService {
    double price(int numOfGuests);
    double price(int numOfGuests, boolean addBreakfast);
}
