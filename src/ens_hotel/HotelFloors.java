package ens_hotel;

public enum HotelFloors {
    FIRST(5),
    SECOND(6),
    THIRD(8),
    FOURTH;
    int roomsPerFloor;

    HotelFloors(int roomsPerFloor) {
        this.roomsPerFloor = roomsPerFloor;
    }
}
