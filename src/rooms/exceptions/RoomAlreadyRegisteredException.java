package rooms.exceptions;

public class RoomAlreadyRegisteredException extends Exception {
    public RoomAlreadyRegisteredException(String message){
       super(message);
    }
}
