package rooms;

public class Room {
    private int roomId;
    private int roomNumber;
    private int floorNumber;
    private int capacity;
    private String kind;
    private int rate;
    private String roomCode;
    private int numberOfGuest;

    private boolean isAvailable;

    private boolean isBooked = false;

    public boolean getAvailabilityStatus() {
        return isAvailable;
    }
    public boolean getBookedStatus() {
        return this.isBooked;
    }
    /*Returns new status */
    public void setAvailablityStatus(boolean availabilityStatus){
        this.isAvailable = availabilityStatus;
    }
    public void setBookedStatus(boolean bookedStatus){
        this.isBooked = bookedStatus;
    }
    public void setNumberOfGuest(int numOfGuest){
        this.numberOfGuest = numOfGuest;
    }
    public int getNumberOfGuest(int numOfGuest){
        return numOfGuest;
    }
    public  Room(
            int roomId,
            int roomNumber,
            int floorNumber,
            int capacity,
            String kind,
            String roomCode,
            int rate
            ){
       this.roomId = roomId;
       this.roomNumber = roomNumber;
       this.floorNumber = floorNumber;
       this.capacity = capacity;
       this.kind = kind;
       this.roomCode = roomCode;
       this.rate = rate;
    }
}
